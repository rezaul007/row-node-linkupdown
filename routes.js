/** dependencies */
const { sampleRoute } = require("./handler/routeHandler/sampleRouteHandler");
const { userRouteHandler } = require("./handler/routeHandler/userRouteHandler");
const { tokenRouteHandler } = require("./handler/routeHandler/tokenRouteHandler");
const { checkRouteHandler } = require("./handler/routeHandler/checkHandler");

const routes = {
  sample: sampleRoute,
  user: userRouteHandler,
  token: tokenRouteHandler,
  check: checkRouteHandler,
};

module.exports = routes;
