/** dependencies */
const server = require("./lib/server");
const worker = require("./lib/worker");

/** app scaffolding */
const app = {};

app.init = () => {
  /** create server */
  server.init();

  /** run worker checks */
  worker.init();
};

app.init();
