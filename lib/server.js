/** dependencies */
const http = require("http");
const { handleReqRes } = require("../helpers/handleReqRes");
const envToExport = require("../helpers/environment");

/** app scaffolding */
const server = {};

/** create server */
server.handleReqRes = handleReqRes;
server.init = () => {
  const serverCreate = http.createServer(server.handleReqRes);
  serverCreate.listen(envToExport.port, () => {
    console.log(`server is running at ${envToExport.port} port`);
  });
};

module.exports = server;
