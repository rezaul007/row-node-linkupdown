/** dependencies */
const lib = require("./write");
const { parseJson } = require("../helpers/utility");
const url = require("url");
const http = require("http");
const https = require("https");
const { sendSms } = require("../helpers/notification");

/** app scaffolding */
const worker = {};

worker.sentNotification = (updatedCheckData) => {
  const msg = `Alert: your check for ${updatedCheckData.method.toUpperCase()} ${updatedCheckData.protocol}://${updatedCheckData.url}} is currently ${updatedCheckData.status}`;
  sendSms(updatedCheckData.phone, msg, (err) => {
    if (!err) {
      console.log(`sms send to ${updatedCheckData.phone}`);
    } else {
      console.log("Can't send notification sms !!!", err);
    }
  });
};

worker.storeCheckOutcome = (parsedCheck, checkoutCome) => {
  const status = !checkoutCome.error && checkoutCome.statusCode && parsedCheck.successCode.includes(checkoutCome.statusCode) ? "up" : "down";
  const alertWanted = parsedCheck.lastCheck && parsedCheck.status !== status ? true : false;
  console.log("alertWanted", alertWanted);

  const updatedCheckData = {
    ...parsedCheck,
    status: status,
    lastCheck: Date.now(),
  };
  lib.update("checks", updatedCheckData.checkId, updatedCheckData, (err) => {
    if (!err) {
      if (alertWanted) {
        worker.sentNotification(updatedCheckData);
      } else {
        console.log(`No need to send sms for ${updatedCheckData.checkId} id `);
      }
    } else {
      console.log("can't update check data !!!");
    }
  });
};

worker.performCheck = (parsedCheck) => {
  let checkoutCome = {
    error: false,
    statusCode: false,
  };
  let outcomeSent = false;
  const parseUrl = url.parse(`${parsedCheck.protocol}://${parsedCheck.url}`, true);
  const hostName = parseUrl.hostname;
  const path = parseUrl.path;
  const requestObj = {
    protocol: parsedCheck.protocol + ":",
    hostname: hostName,
    method: parsedCheck.method.toUpperCase(),
    path: path,
    timeout: parsedCheck.timeOutSeconds * 1000,
  };
  const chooseProtocol = parsedCheck.protocol === "http" ? http : https;
  const req = chooseProtocol.request(requestObj, (res) => {
    const status = res.statusCode;
    if (!outcomeSent) {
      outcomeSent = true;
      checkoutCome.statusCode = status;
      worker.storeCheckOutcome(parsedCheck, checkoutCome);
    }
  });
  req.on("error", (e) => {
    if (!outcomeSent) {
      outcomeSent = true;
      checkoutCome = {
        error: true,
        value: e,
      };
      worker.storeCheckOutcome(parsedCheck, checkoutCome);
    }
  });
  req.on("timeout", (e) => {
    if (!outcomeSent) {
      outcomeSent = true;
      checkoutCome = {
        error: true,
        value: "time out",
      };
      worker.storeCheckOutcome(parsedCheck, checkoutCome);
    }
  });
  req.end();
};

worker.validation = (originalCheckData) => {
  const parsedCheck = parseJson(originalCheckData);
  if (parsedCheck && parsedCheck.checkId) {
    // add additional two data in check data file.
    parsedCheck.status = typeof parsedCheck.status === "string" && ["up", "down"].includes(parsedCheck.status) ? parsedCheck.status : "down";
    parsedCheck.lastCheck = typeof parsedCheck.lastCheck && parsedCheck.lastCheck > 0 ? parsedCheck.lastCheck : false;
    worker.performCheck(parsedCheck);
  } else {
    console.log("check validation error !!!");
  }
};

worker.gatherChecks = () => {
  lib.list("checks", (err, checks) => {
    if (!err && checks && checks.length > 0) {
      checks.forEach((check) => {
        lib.read("checks", check, (err2, originalCheckData) => {
          if (!err2 && originalCheckData) {
            worker.validation(originalCheckData);
          } else {
            console.log("Couldn't find checks from check file  !!!");
          }
        });
      });
    } else {
      console.log("Couldn't find checks from list !!!");
    }
  });
};

worker.loop = () => {
  setInterval(() => {
    worker.gatherChecks();
  }, 1000 * 60);
};

worker.init = () => {
  worker.gatherChecks();

  // worker.loop();
};

module.exports = worker;
