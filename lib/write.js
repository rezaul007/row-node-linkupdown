/** dependencies  */
const fs = require("fs");
const path = require("path");

/** app scaffolding */
const lib = {};

lib.baseurl = path.join(__dirname, "/../.data/");

lib.create = (dir, file, data, callBack) => {
  fs.open(`${lib.baseurl + dir}/${file}.json`, "wx", (err, fileDescriptor) => {
    if (!err && fileDescriptor) {
      const jsonStringData = JSON.stringify(data);
      fs.writeFile(fileDescriptor, jsonStringData, (err2) => {
        if (!err2) {
          fs.close(fileDescriptor, (err3) => {
            if (!err3) {
              callBack(false);
            } else {
              callBack(err3);
            }
          });
        } else {
          callBack(err2);
        }
      });
    } else {
      callBack(err);
    }
  });
};

lib.read = (dir, file, callBack) => {
  fs.readFile(`${lib.baseurl + dir}/${file}.json`, "utf8", (err, data) => {
    callBack(err, data);
  });
};

lib.update = (dir, file, data, callBack) => {
  fs.open(`${lib.baseurl + dir}/${file}.json`, "r+", (err, fileDescriptor) => {
    if (!err) {
      const jsonStringData = JSON.stringify(data);
      fs.ftruncate(fileDescriptor, (err2) => {
        if (!err) {
          fs.writeFile(fileDescriptor, jsonStringData, (err3) => {
            if (!err3) {
              fs.close(fileDescriptor, (err4) => {
                if (!err4) {
                  callBack(false);
                } else {
                  callBack(err4);
                }
              });
            } else {
              callBack(err3);
            }
          });
        } else {
          callBack(err2);
        }
      });
    } else {
      callBack(err);
    }
  });
};

lib.delete = (dir, file, callBack) => {
  fs.unlink(`${lib.baseurl + dir}/${file}.json`, (err) => {
    if (!err) {
      callBack(false);
    } else {
      callBack(err);
    }
  });
};

lib.list = (dir, callBack) => {
  fs.readdir(`${lib.baseurl + dir}`, (err, fileList) => {
    if (!err && fileList && fileList.length > 0) {
      const fileNameList = [];
      fileList.forEach((file) => {
        fileNameList.push(file.replace(".json", ""));
      });
      callBack(false, fileNameList);
    } else {
      callBack("list not found !!!");
    }
  });
};

module.exports = lib;
