/** dependencies */
const url = require("url");
const { StringDecoder } = require("string_decoder");
const routes = require("../routes");
const { notFoundRoute } = require("../handler/routeHandler/notFoundRouteHandler");
const { parseJson } = require("../helpers/utility");

/** app scaffolding */
const handler = {};

handler.handleReqRes = (req, res) => {
  const parsedUrl = url.parse(req.url, true);
  const path = parsedUrl.pathname;
  const trimmedPath = path.replace(/^\/+|\/+$/g, "");
  const queryStringObj = parsedUrl.query;
  const headerObj = req.headers;
  const method = req.method.toLowerCase();
  const decoder = new StringDecoder("utf-8");
  let realData = "";

  req.on("data", (buffer) => {
    realData += decoder.write(buffer);
  });
  req.on("end", () => {
    realData += decoder.end();
    const chooseHandler = routes[trimmedPath] ? routes[trimmedPath] : notFoundRoute;
    const body = parseJson(realData);
    chooseHandler({ path, queryStringObj, headerObj, method, body }, (statusCode, payload) => {
      const status = typeof statusCode === "number" ? statusCode : 500;
      const response = typeof payload === "object" ? payload : { message: "internal server error" };
      const responseString = JSON.stringify(response);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(status);
      res.end(responseString);
    });
  });
}; 

module.exports = handler;
