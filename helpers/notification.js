const envToExport = require("./environment");
const client = require("twilio")(envToExport.twilio.accountSid, envToExport.twilio.authToken);

const notification = {};

notification.sendSms = (phone, text, callback) => {
  const to = typeof phone === "string" && phone.length === 11 ? phone : false;
  const body = typeof text === "string" && text.trim().length > 0 ? text : false;
  if (to && body) {
    const option = {
      body,
      from: envToExport.twilio.from,
      to: `+88${to}`,
    };
    client.messages
      .create(option)
      .then((message) => callback(false))
      .catch((err) => callback(err));
  } else {
    callback("phone or message body not found !!!");
  }
};

module.exports = notification;
