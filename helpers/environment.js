const environments = {};

environments.staging = {
  port: 3000,
  envName: "stage",
  secretKey: "xsjbcjcbjdbvjdbvj",
  twilio: {
    from: "+16814914792",
    accountSid: "AC99ecbce2807f3942ed75097056a7fb0a",
    authToken: "88b09567dc22c707d98486208f25a2f5",
  },
};
environments.production = {
  port: 5000,
  envName: "production",
  secretKey: "tyoinfjwhvghvwhvjw",
  twilio: {
    from: "+16814914792",
    accountSid: "AC99ecbce2807f3942ed75097056a7fb0a",
    authToken: "88b09567dc22c707d98486208f25a2f5",
  },
};

const currentEnv = typeof process.env.NODE_ENV === "string" ? process.env.NODE_ENV : "staging";
const envToExport = typeof environments[currentEnv] === "object" ? environments[currentEnv] : environments.staging;

module.exports = envToExport;
