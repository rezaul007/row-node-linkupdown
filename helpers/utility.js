const crypto = require("crypto");
const environment = require("../helpers/environment");

/** module scaffolding */
const utility = {};

utility.parseJson = (jsonString) => {
  let output = {};
  try {
    output = JSON.parse(jsonString);
  } catch {
    output = {};
  }
  return output;
};

utility.hash = (str) => {
  if (typeof str === "string" && str.length > 0) {
    const hash = crypto.createHmac("sha256", environment.secretKey).update(str).digest("hex");
    return hash;
  } else {
    return false;
  }
};

utility.createRandomStr = (strlen) => {
  let length = typeof strlen === "number" && strlen > 0 ? strlen : false;
  if (length) {
    const possibleChar = "abcdefghijklmnopqrstuvwxyz1234567890";
    let output = "";
    for (let i = 0; i < length; i++) {
      output += possibleChar.charAt(Math.floor(Math.random() * possibleChar.length));
    }
    return output;
  } else {
    return false;
  }
};

module.exports = utility;
