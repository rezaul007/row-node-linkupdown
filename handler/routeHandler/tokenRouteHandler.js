const lib = require("../../lib/write");
const { hash, parseJson, createRandomStr } = require("../../helpers/utility");

/** Module scaffolding */
const handler = {};
handler._token = {};

handler.tokenRouteHandler = (requestProperty, callBack) => {
  const acceptMethods = ["get", "post", "put", "delete"];
  if (acceptMethods.indexOf(requestProperty.method) > -1) {
    handler._token[requestProperty.method](requestProperty, callBack);
  } else {
    callBack(405);
  }
};

handler._token.post = (requestProperty, callBack) => {
  const phone = typeof requestProperty.body.phone === "string" && requestProperty.body.phone.trim().length === 11 ? requestProperty.body.phone : false;
  const password = typeof requestProperty.body.password === "string" && requestProperty.body.password.trim().length > 0 ? requestProperty.body.password : false;

  if (phone && password) {
    lib.read("users", phone, (err1, userData) => {
      if (hash(password) === parseJson(userData)?.password) {
        const token = createRandomStr(16);
        const tokenObj = {
          token,
          phone,
          expires: Date.now() * 60 * 60 * 1000,
        };
        lib.create("tokens", token, tokenObj, (err2) => {
          if (!err2) {
            callBack(200, tokenObj);
          } else {
            callBack(500, { message: "Internal server file" });
          }
        });
      } else {
        callBack(404, { message: "No user found !!!" });
      }
    });
  } else {
    callBack(400, { message: "invalid request" });
  }
};
handler._token.get = (requestProperty, callBack) => {
  const id = typeof requestProperty.queryStringObj.id === "string" && requestProperty.queryStringObj.id.trim().length === 16 ? requestProperty.queryStringObj.id : false;
  if (id) {
    lib.read("tokens", id, (err, tokenData) => {
      const token = { ...parseJson(tokenData) };
      if (!err && token) {
        callBack(200, token);
      } else {
        callBack(404, { message: "token not found" });
      }
    });
  } else {
    callBack(404, { message: "id not found" });
  }
};
handler._token.put = (requestProperty, callBack) => {
  const id = typeof requestProperty.body.id === "string" && requestProperty.body.id.trim().length === 16 ? requestProperty.body.id : false;
  const expires = typeof requestProperty.body.expires === "boolean" && requestProperty.body.expires === true ? requestProperty.body.expires : false;
  if (id && expires) {
    lib.read("tokens", id, (err, tokenData) => {
      if (!err && tokenData) {
        const token = { ...JSON.parse(tokenData) };
        if (token.expires > Date.now()) {
          token.expires = Date.now() * 60 * 60 * 1000;
          lib.update("tokens", id, token, (err2) => {
            if (!err2) {
              callBack(200, token);
            } else {
              callBack(500, { message: "Can't update token expires time" });
            }
          });
        } else {
          callBack(403, { message: "token already expired !!!" });
        }
      } else {
        callBack(404, { message: "token not found !!!" });
      }
    });
  } else {
    callBack(400, { message: "bad request !!!" });
  }
};
handler._token.delete = (requestProperty, callBack) => {
  const id = typeof requestProperty.queryStringObj.id === "string" && requestProperty.queryStringObj.id.trim().length === 16 ? requestProperty.queryStringObj.id : false;
  if (id) {
    lib.read("tokens", id, (err, data) => {
      if (!err && data) {
        lib.delete("tokens", id, (err2) => {
          if (!err2) {
            callBack(200, { message: "token deleted successfully" });
          } else {
            callBack(500, { message: "internat server error !!!" });
          }
        });
      } else {
        callBack(500, { message: "internat server error !!!" });
      }
    });
  } else {
    callBack(404, { message: "id not found" });
  }
};

handler._token.verify = (id, phone, callback) => {
  lib.read("tokens", id, (err, tokenData) => {
    if (!err && parseJson(tokenData)?.phone === phone && parseJson(tokenData)?.expires > Date.now()) {
      callback(true);
    } else {
      callback(false);
    }
  });
};

module.exports = handler;
