const handle = {};

handle.sampleRoute = (requestProperties, callBack) => {
  console.log(requestProperties);
  callBack(200, { message: "sample route" });
};

module.exports = handle;
