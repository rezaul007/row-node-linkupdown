const lib = require("../../lib/write");
const { parseJson, createRandomStr } = require("../../helpers/utility");
const { _token } = require("./tokenRouteHandler");
const { userRouteHandler } = require("./userRouteHandler");

/** Module scaffolding */
const handler = {};
handler._check = {};

handler.checkRouteHandler = (requestProperty, callBack) => {
  const acceptMethods = ["get", "post", "put", "delete"];
  if (acceptMethods.indexOf(requestProperty.method) > -1) {
    handler._check[requestProperty.method](requestProperty, callBack);
  } else {
    callBack(405);
  }
};

handler._check.post = (requestProperty, callBack) => {
  const method = typeof requestProperty.body.method === "string" && ["post", "get", "put", "delete"].includes(requestProperty.body.method) ? requestProperty.body.method : false;
  const url = typeof requestProperty.body.url === "string" && requestProperty.body.url.trim().length > 0 ? requestProperty.body.url : false;
  const protocol = typeof requestProperty.body.protocol === "string" && ["http", "https"].includes(requestProperty.body.protocol) ? requestProperty.body.protocol : false;
  const successCode = typeof requestProperty.body.successCode === "object" && requestProperty.body.successCode instanceof Array ? requestProperty.body.successCode : false;
  const timeOutSeconds = typeof requestProperty.body.timeOutSeconds === "number" && requestProperty.body.timeOutSeconds % 1 === 0 ? requestProperty.body.timeOutSeconds : false;
  const phone = typeof requestProperty.body.phone === "string" && requestProperty.body.phone.trim().length === 11 ? requestProperty.body.phone : false;

  if (method && url && protocol && successCode && timeOutSeconds && phone) {
    const token = typeof requestProperty?.headerObj?.token === "string" ? requestProperty?.headerObj?.token : false;
    lib.read("tokens", token, (err, tokenData) => {
      if (!err && tokenData) {
        const phone = parseJson(tokenData).phone;
        lib.read("users", phone, (err0, userData) => {
          if (!err0 && userData) {
            _token.verify(token, phone, (isValidToken) => {
              if (isValidToken) {
                const parsedUserData = { ...parseJson(userData) };
                const userCheck = typeof parsedUserData.checks === "object" && parsedUserData.checks instanceof Array ? parsedUserData.checks : [];
                if (userCheck && userCheck.length < 5) {
                  const checkId = createRandomStr(8);
                  const checkObj = {
                    checkId,
                    method,
                    url,
                    protocol,
                    successCode,
                    timeOutSeconds,
                    phone,
                  };
                  lib.create("checks", checkId, checkObj, (err4) => {
                    if (!err4) {
                      userCheck.push(checkId);
                      parsedUserData.checks = userCheck;
                      lib.update("users", phone, parsedUserData, (err5) => {
                        if (!err5) {
                          callBack(200, { message: "check added successfully" });
                        } else {
                          callBack(500, { message: "Interval server error !!!" });
                        }
                      });
                    } else {
                      callBack(500, { message: "Interval server error !!!" });
                    }
                  });
                } else {
                  callBack(401, { message: "user not allowed add check !!!" });
                }
              } else {
                callBack(403, { message: "Unauthorized error !!!" });
              }
            });
          } else {
            callBack(500, { message: "Interval server error !!!" });
          }
        });
      } else {
        callBack(400, { message: "in  Invalid request" });
      }
    });
  } else {
    callBack(400, { message: "IN invalid request" });
  }
};
handler._check.get = (requestProperty, callBack) => {
  const id = typeof requestProperty.queryStringObj.id === "string" && requestProperty.queryStringObj.id.trim().length === 8 ? requestProperty.queryStringObj.id : false;
  if (id) {
    lib.read("checks", id, (err, checkData) => {
      if (!err && checkData) {
        const token = typeof requestProperty?.headerObj?.token === "string" ? requestProperty?.headerObj?.token : false;
        _token.verify(token, parseJson(checkData)?.phone, (isValidToken) => {
          if (isValidToken) {
            callBack(200, { message: "check found", data: parseJson(checkData) });
          } else {
            callBack(403, { message: "Unauthorized error !!!" });
          }
        });
      } else {
        callBack(404, { message: "check not found !!!" });
      }
    });
  } else {
    callBack(400, { message: "Invalid request !!!" });
  }
};
handler._check.put = (requestProperty, callBack) => {
  const method = typeof requestProperty.body.method === "string" && ["post", "get", "put", "delete"].includes(requestProperty.body.method) ? requestProperty.body.method : false;
  const url = typeof requestProperty.body.url === "string" && requestProperty.body.url.trim().length > 0 ? requestProperty.body.url : false;
  const protocol = typeof requestProperty.body.protocol === "string" && ["http", "https"].includes(requestProperty.body.protocol) ? requestProperty.body.protocol : false;
  const successCode = typeof requestProperty.body.successCode === "object" && requestProperty.body.successCode instanceof Array ? requestProperty.body.successCode : false;
  const timeOutSeconds = typeof requestProperty.body.timeOutSeconds === "number" && requestProperty.body.timeOutSeconds % 1 === 0 ? requestProperty.body.timeOutSeconds : false;
  const phone = typeof requestProperty.body.phone === "string" && requestProperty.body.phone.trim().length === 11 ? requestProperty.body.phone : false;
  const id = typeof requestProperty.body.id === "string" && requestProperty.body.id.trim().length > 0 ? requestProperty.body.id : false;

  if (id) {
    if (phone || url || protocol || successCode || timeOutSeconds || method) {
      lib.read("checks", id, (err, checkData) => {
        if (!err && checkData) {
          const parsedCheckData = { ...JSON.parse(checkData) };
          const token = typeof requestProperty?.headerObj?.token === "string" ? requestProperty?.headerObj?.token : false;
          if (token) {
            _token.verify(token, parsedCheckData?.phone, (isVerifyToken) => {
              if (isVerifyToken) {
                if (method) parsedCheckData.method = method;
                if (url) parsedCheckData.url = url;
                if (successCode) parsedCheckData.successCode = successCode;
                if (timeOutSeconds) parsedCheckData.timeOutSeconds = timeOutSeconds;
                if (phone) parsedCheckData.phone = phone;
                if (protocol) parsedCheckData.protocol = protocol;
                lib.update("checks", id, parsedCheckData, (err2) => {
                  if (!err2) {
                    callBack(200, { message: "check update successfully" });
                  } else {
                    callBack(500, { message: "Server error !!!" });
                  }
                });
              } else {
                callBack(403, { message: "token expired !!!" });
              }
            });
          } else {
            callBack(403, { message: "Unauthorized error !!!" });
          }
        } else {
          callBack(404, { message: "not found" });
        }
      });
    } else {
      callBack(400, { message: "bad request" });
    }
  } else {
    callBack(400, { message: "bad request" });
  }
};
handler._check.delete = (requestProperty, callBack) => {
  const id = typeof requestProperty.queryStringObj.id === "string" && requestProperty.queryStringObj.id.trim().length === 8 ? requestProperty.queryStringObj.id : false;
  if (id) {
    lib.read("checks", id, (err, checkData) => {
      if (!err && checkData) {
        const token = typeof requestProperty?.headerObj?.token === "string" ? requestProperty?.headerObj?.token : false;
        _token.verify(token, parseJson(checkData)?.phone, (isValidToken) => {
          if (isValidToken) {
            lib.delete("checks", id, (err2) => {
              if (!err2) {
                lib.read("users", parseJson(checkData)?.phone, (err3, user) => {
                  if (!err3 && user) {
                    const userData = { ...parseJson(user) };
                    const userChecks = typeof userData.checks === "object" && userData.checks instanceof Array ? userData.checks : [];
                    const findIndex = userChecks.indexOf(id);
                    if (findIndex > -1) {
                      userChecks.splice(findIndex, 1);
                      userData.checks = [...userChecks];
                      lib.update("users", userData.phone, userData, (err4) => {
                        if (!err4) {
                          callBack(200, { message: "check deleted successfully" });
                        } else {
                          callBack(500, { message: "Server error !!!" });
                        }
                      });
                    } else {
                      callBack(500, { message: "Server error !!!" });
                    }
                  } else {
                    callBack(500, { message: "Server error !!!" });
                  }
                });
              } else {
                callBack(500, { message: "Server error !!!" });
              }
            });
          } else {
            callBack(403, { message: "Unauthorized error !!!" });
          }
        });
      } else {
        callBack(404, { message: "check not found !!!" });
      }
    });
  } else {
    callBack(400, { message: "Invalid request !!!" });
  }
};

module.exports = handler;
