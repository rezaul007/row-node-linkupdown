const lib = require("../../lib/write");
const { hash, parseJson } = require("../../helpers/utility");
const { _token } = require("./tokenRouteHandler");

/** Module scaffolding */
const handler = {};
handler._user = {};

handler.userRouteHandler = (requestProperty, callBack) => {
  const acceptMethods = ["get", "post", "put", "delete"];
  if (acceptMethods.indexOf(requestProperty.method) > -1) {
    handler._user[requestProperty.method](requestProperty, callBack);
  } else {
    callBack(405);
  }
};

handler._user.post = (requestProperty, callBack) => {
  const firstName = typeof requestProperty.body.firstName === "string" && requestProperty.body.firstName.trim().length > 0 ? requestProperty.body.firstName : false;
  const lastName = typeof requestProperty.body.lastName === "string" && requestProperty.body.lastName.trim().length > 0 ? requestProperty.body.lastName : false;
  const phone = typeof requestProperty.body.phone === "string" && requestProperty.body.phone.trim().length === 11 ? requestProperty.body.phone : false;
  const password = typeof requestProperty.body.password === "string" && requestProperty.body.password.trim().length > 0 ? requestProperty.body.password : false;
  const tosAgreement = typeof requestProperty.body.tosAgreement === "boolean" ? requestProperty.body.tosAgreement : false;

  if (firstName && lastName && phone && password && tosAgreement) {
    lib.read("users", phone, (err1) => {
      if (err1) {
        const data = {
          firstName,
          lastName,
          phone,
          tosAgreement,
          password: hash(password),
        };
        lib.create("users", phone, data, (err2) => {
          if (!err2) {
            callBack(200, { message: "user create successfully !!!" });
          } else {
            callBack(500, { message: "can't create file" });
          }
        });
      } else {
        callBack(500, { message: "user already exist" });
      }
    });
  } else {
    callBack(400, { message: "invalid request" });
  }
};

handler._user.get = (requestProperty, callBack) => {
  const phone = typeof requestProperty.queryStringObj.phone === "string" && requestProperty.queryStringObj.phone.trim().length === 11 ? requestProperty.body.phone : false;
  if (phone) {
    const token = typeof requestProperty?.headerObj?.token === "string" ? requestProperty?.headerObj?.token : false;
    if (token) {
      _token.verify(token, phone, (tokenData) => {
        if (tokenData) {
          lib.read("users", phone, (err, user) => {
            if (!err) {
              const copyUser = { ...parseJson(user) };
              delete copyUser.password;
              callBack(200, { message: "user found", data: copyUser });
            } else {
              callBack(404, { message: "user not found" });
            }
          });
        } else {
          callBack(403, { message: "token expired !!!" });
        }
      });
    } else {
      callBack(403, { message: "Unauthorized error !!!" });
    }
  } else {
    callBack(404, { message: "not found" });
  }
};
handler._user.put = (requestProperty, callBack) => {
  const firstName = typeof requestProperty.body.firstName === "string" && requestProperty.body.firstName.trim().length > 0 ? requestProperty.body.firstName : false;
  const lastName = typeof requestProperty.body.lastName === "string" && requestProperty.body.lastName.trim().length > 0 ? requestProperty.body.lastName : false;
  const phone = typeof requestProperty.body.phone === "string" && requestProperty.body.phone.trim().length === 11 ? requestProperty.body.phone : false;
  const password = typeof requestProperty.body.password === "string" && requestProperty.body.password.trim().length > 0 ? requestProperty.body.password : false;
  if (phone) {
    lib.read("users", phone, (err, user) => {
      if (!err) {
        const userData = { ...JSON.parse(user) };
        if (firstName || lastName || password) {
          const token = typeof requestProperty?.headerObj?.token === "string" ? requestProperty?.headerObj?.token : false;
          if (token) {
            _token.verify(token, phone, (tokenData) => {
              if (tokenData) {
                if (firstName) userData.firstName = firstName;
                if (lastName) userData.lastName = lastName;
                if (password) userData.password = hash(password);
                lib.update("users", phone, userData, (err2) => {
                  if (!err2) {
                    callBack(200, { message: "user update successfully" });
                  }
                });
              } else {
                callBack(403, { message: "token expired !!!" });
              }
            });
          } else {
            callBack(403, { message: "Unauthorized error !!!" });
          }
        } else {
          callBack(400, { message: "bad request" });
        }
      } else {
        callBack(404, { message: "not found" });
      }
    });
  } else {
    callBack(400, { message: "bad request" });
  }
};
handler._user.delete = (requestProperty, callBack) => {
  const phone = typeof requestProperty.queryStringObj.phone === "string" && requestProperty.queryStringObj.phone.trim().length === 11 ? requestProperty.body.phone : false;
  if (phone) {
    const token = typeof requestProperty?.headerObj?.token === "string" ? requestProperty?.headerObj?.token : false;
    if (token) {
      _token.verify(token, phone, (tokenData) => {
        if (tokenData) {
          lib.read("users", phone, (err, data) => {
            if (!err && data) {
              lib.delete("users", phone, (err2) => {
                if (!err2) {
                  callBack(200, { message: "user delete successfully" });
                } else {
                  callBack(500, { message: "internat server error !!!" });
                }
              });
            } else {
              callBack(500, { message: "internat server error !!!" });
            }
          });
        } else {
          callBack(403, { message: "token expired !!!" });
        }
      });
    } else {
      callBack(403, { message: "Unauthorized error !!!" });
    }
  } else {
    callBack(404, { message: "not found" });
  }
};

module.exports = handler;
