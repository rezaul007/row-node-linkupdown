/** app scaffolding */
const handle = {};

handle.notFoundRoute = (requestProperties, callBack) => {
  console.log(requestProperties);
  callBack(404, { message: "not found route" });
};

module.exports = handle;
